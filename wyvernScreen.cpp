#include <format>
#include <string>

#include "wyvernScreen.hpp"

void wyvernScreen::updateTimingString(int64_t timing) { timingString = std::to_string(timing); }

void wyvernScreen::updateScoreString(uint64_t timing) { scoreString = std::format("Score: {}", timing); }

void wyvernScreen::updateFpsString(uint64_t fps) { fpsString = std::format("FPS: {}", fps); }

void wyvernScreen::render() { scr.render(); }
