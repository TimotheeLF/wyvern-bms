.POSIX:
.SUFFIXES:

CXX                 = clang
CXXFLAGS            = -std=c++20 -pthread -stdlib=libc++ -fexperimental-library
LDFLAGS             = -lSDL2 -lSDL2_ttf -lSDL2_image -lSDL2_mixer -fuse-ld=lld -lm -lc++ -lc++abi -lc++experimental
RELEASECXXFLAGS     = $(CXXFLAGS) -O3 -march=native
DEBUGCXXFLAGS       = $(CXXFLAGS) -O0 -march=native -g -D_LIBCPP_ENABLE_ASSERTIONS=1

OBJ                 = main.o bmsMap.o hitPng.o sdlScreen.o scrollMap.o wyvernScreen.o
PREFIX              = /usr/local

all: wyvern

install: wyvern
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f wyvern-bms $(DESTDIR)$(PREFIX)/bin

wyvern: $(OBJ)
	$(CXX) $(LDFLAGS) -o wyvern-bms $(OBJ)

main.o: main.cpp
bmsMap.o: bmsMap.cpp bmsMap.hpp
hitPng.o: hitPng.cpp hitPng.hpp
scrollMap.o: scrollMap.cpp scrollMap.hpp
sdlScreen.o: sdlScreen.cpp sdlScreen.hpp
wyvernScreen.o: wyvernScreen.cpp wyvernScreen.hpp

debug: RELEASECXXFLAGS = $(DEBUGCXXFLAGS)
debug: $(OBJ)
	$(CXX) $(LDFLAGS) -o wyvern-bms $(OBJ) $(DEBUGCXXFLAGS)

clean:
	rm -f wyvern-bms main.o bmsmap.o hit.o

.SUFFIXES: .cpp .o
.cpp.o:
	$(CXX) $(RELEASECXXFLAGS) -c $<
