#include <algorithm>
#include <array>
#include <chrono>
#include <cmath>
#include <filesystem>

#include <stdint.h>

#include "definitions.hpp"
#include "scrollMap.hpp"

constexpr std::array<int, 8> lanePosition = { 50, 150, 250, 350, 450, 550, 650, 750 };
constexpr int redBarHeight = 980;

scrollMap::scrollMap()
    : bpm { 130 } // Default values following the spec
    , spb { 60.0f / 130.0f }
    , spm { spb / 4.0f }
    , keepGoing { true }
    , currentkb { SDL_GetKeyboardState(nullptr) }
    , oldKb { 0 }
    , score { 0 } { }

void scrollMap::processKeyNoteClick(int lane) {
    for (const audioChunk &audioStuff : audioChunksList) {
        if (audioStuff.decodedBase16WavNumber == noteList[0].wavNumber) {
            Mix_PlayChannel(-1, audioStuff.audioChunkForPlaying,
                            0); // -1 channel = The first available, 0 loops = play once
        }
    }

    wScreen.updateTimingString(diffTiming);

    // TODO: Better score?
    score += diffTiming > 0 && diffTiming < 100 ? (100 - diffTiming) : 0;
    wScreen.updateScoreString(score);

    hits.computeSurface(lane, SDL_GetTicks());
}

// TODO: Automatically delete notes that go off-screen
void scrollMap::processKeyHit(unsigned char lane) {
    // We save the current keyboard state to avoid repeating keys
    // in the next frame.
    oldKb |= (1 << lane);

    char count = 0;
    extractTimings();

    // Verify the last 8 notes
    for (auto it = noteList.begin(); count < 8; it++) {
        if (verifyIfValidHit(*it, lane)) {
            processKeyNoteClick(lane);
            noteList.erase(noteList.begin() + count);

            if (noteList.empty()) {
                keepGoing = false;
                break;
            }
        }
        count++;
    }
}

bool scrollMap::verifyIfValidHit(const note &no, unsigned char lane) {
    // Allow mashing, TODO: Make that impossible
    if (!(noteList[0].position & (1 << lane)))
        return false;

    diffTiming = std::abs(static_cast<int64_t>((no.measure * spm) * 1000) - timeSinceStart.count());

    return diffTiming < 250;
}

void scrollMap::extractTimings() {
    currentTime = hClock::now();
    timeSinceStart = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - startTime);
}

void scrollMap::processKeyboard() {
    SDL_PumpEvents();

    if (currentkb[SDL_SCANCODE_Q])
        keepGoing = false;
    else {
        if (!(oldKb & (1 << 0)) && currentkb[SDL_SCANCODE_LSHIFT])
            processKeyHit(0);
        if (!(oldKb & (1 << 1)) && currentkb[SDL_SCANCODE_S])
            processKeyHit(1);
        if (!(oldKb & (1 << 2)) && currentkb[SDL_SCANCODE_D])
            processKeyHit(2);
        if (!(oldKb & (1 << 3)) && currentkb[SDL_SCANCODE_F])
            processKeyHit(3);
        if (!(oldKb & (1 << 4)) && currentkb[SDL_SCANCODE_SPACE])
            processKeyHit(4);
        if (!(oldKb & (1 << 5)) && currentkb[SDL_SCANCODE_J])
            processKeyHit(5);
        if (!(oldKb & (1 << 6)) && currentkb[SDL_SCANCODE_K])
            processKeyHit(6);
        if (!(oldKb & (1 << 7)) && currentkb[SDL_SCANCODE_L])
            processKeyHit(7);
    }
}

void scrollMap::sortNotes() {
    std::sort(noteList.begin(), noteList.end(),
              [](const note &note1, const note &note2) { return note1.measure > note2.measure; });
}

void scrollMap::pushNote(const note &no) { noteList.push_back(no); }

void scrollMap::pushName(const name &na) { nameList.push_back(na); }

void scrollMap::pushAudioChunk(const audioChunk &au) { audioChunksList.push_back(au); }

void scrollMap::pushHitTexture(SDL_Texture *texture) { hits.pushHitTexture(texture); }

const std::vector<note> &scrollMap::notes() { return noteList; }

const std::vector<name> &scrollMap::names() { return nameList; }
