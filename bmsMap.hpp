#pragma once

/*
    Class used to parse the .bms file and to add fill the scrollMap's vectors.

    It also interfaces with the user to get the desired level.
*/

#include <string>
#include <vector>

#include <limits.h>
#include <stddef.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>

#include "definitions.hpp"
#include "scrollMap.hpp"

class bmsMap {
public:
    explicit bmsMap(int scrollingSpeed);

    void getMapName();
    void loadNewMap();
    void loadSkin();

    static int convertChannelToLane(int channel);

    void handleNote(const std::string &line);
    void handleBpm(const std::string &line);
    void handleWav(size_t startIdx);

    void addWavNameToVector(const std::string &line);

private:
    sdlScreen screen;
    scrollMap map;

    char cwd[PATH_MAX];
    std::string fullBmsPath;

    int lanePosition[8];
    int numberOfWavs;
};
