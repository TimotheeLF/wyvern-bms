#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include <string>

#include <stdint.h>

#define LANE1 7
#define LANE2 6
#define LANE3 5
#define LANE4 4
#define LANE5 3
#define LANE6 2
#define LANE7 1
#define LANE8 0

using hClock = std::chrono::high_resolution_clock;

struct note {
    SDL_Rect rectNote = { 0, 0, 0, 0 };
    int wavNumber;
    float measure;
    uint8_t position;
    bool bgm = false; // If true, there is no notes, so we don't care about rectNote's size.
};

struct name {
    std::string nameOfWav;
    int decodedBase16WavNumber; // TODO: Base 36?
};

struct audioChunk {
    Mix_Chunk *audioChunkForPlaying;
    int decodedBase16WavNumber;
};