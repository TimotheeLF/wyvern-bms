# Wyvern BMS
Experimental open source BMS player made in C++23. It makes use of modern features such as smart pointers to avoid memory bugs.

Also because alternatives are kinda meh.

To compile, you need:
* LLVM-clang  (>= 15)
* LLVM-libc++ (>= 15)
* LLVM-lld    (>= 15)
* SDL2-devel
* SDL2-ttf
* SDL2-image
* SDL2-mixer

To debug and develop (optional):
* LLVM-lldb
* Valgrind
* Kcachegrind
* Massif-visualizer
   
To get started, run: 

`make`

To install:

`make install`

To debug:

`make debug` and attach your debugger.

# Structure
Currently, Wyvern is extremely simple. It reads notes line by line using the C++ standard library. 
These are then converted to a `std::vector<note>`. The struct `note` is simply a wrapper around a 
`SDL_Rect`, a measure point and a wav number.

# Defines
* `WYVERN_DEBUG=1` Outputs plenty of information in the console.
* `USE_MMAP_FOR_AUDIO_LOADING=1` Replaces SDL-mixer's audio parser by an mmap alternative.