#define WYVERN_DEBUG
#define USE_MMAP_FOR_AUDIO_LOADING

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>
#include <algorithm>
#include <filesystem>
#include <iostream>
#include <memory>
#include <string>
#include <vector>


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "bmsMap.hpp"
#include "hitPng.hpp"
#include "sdlScreen.hpp"

#define PIXELDELAYACCEPTKEYINPUT 250

typedef std::chrono::high_resolution_clock hClock;

void processKeyNoteClick(int position, int lane);
void processKeyHit(int lane);


SDL_Rect *fpsText = new SDL_Rect({ 1600, 50, 0, 0 });
SDL_Rect *scoreText = new SDL_Rect({ 1600, 150, 0, 0 });
SDL_Rect *timingText = new SDL_Rect({ 900, 400, 0, 0 });

bool processTimingText = false;
bool keepGoing = true;

int score = 0;
constexpr int lanePosition[8] = { 50, 150, 250, 350, 450, 550, 650, 750 };

constexpr SDL_Rect redBar = { 0, 980, 1920, 20 };
std::vector<std::unique_ptr<hit::hitPng>> hits;
std::vector<SDL_Texture *> *hitTextures;
float differenceBetweenPerfectAndHit = 0.0f;
constexpr float adjustedHeightOfRedBar = static_cast<float>(redBar.y) + (static_cast<float>(redBar.h) / 2);


// TTF_RenderText_Blended doesn't like empty strings. :-(
char fpsString[20] = "FPS: ";
char scoreString[20] = "Score: ";
char timingString[20] = "Timing: ";

constexpr int rectanglePixelSpeed = 2000; // Per seconds
constexpr int rectanglePixelSpeedMs = 2; // Per ms
constexpr unsigned int microSecondsDelay
    = static_cast<unsigned int>(1.0f * (1000000.0f / static_cast<float>(rectanglePixelSpeed)));

int main(int argc, char *argv[]) {
    sdlScreen screen;
    bool isFirst = false;
    int fps = 0;
    unsigned int fpsTimer = 0;

    noteList = new std::vector<note>();
    hitTextures = new std::vector<SDL_Texture *>();

    SDL_Surface *surfaceMessageFps = TTF_RenderText_Blended(tuffy, fpsString, { 200, 255, 255, 255 });
    SDL_Texture *messageFps = SDL_CreateTextureFromSurface(renderer, surfaceMessageFps);
    SDL_Surface *surfaceMessageScore = TTF_RenderText_Blended(tuffy, scoreString, { 200, 255, 255, 255 });
    SDL_Texture *messageScore = SDL_CreateTextureFromSurface(renderer, surfaceMessageScore);
    SDL_Surface *surfaceMessageTiming = TTF_RenderText_Blended(tuffy, timingString, { 200, 255, 255, 255 });
    SDL_Texture *messageTiming = SDL_CreateTextureFromSurface(renderer, surfaceMessageTiming);

    // Adds all hit textures to the hit vector. This will be used to render them
    // and share the textures to reduce RAM usage.
    for (const auto &folder : std::filesystem::directory_iterator(std::string(cwd) + "/skin/hit/")) {
        if (folder.path().generic_string().substr(folder.path().generic_string().size() - 4) == ".png") {
            SDL_Surface *surf = IMG_Load(folder.path().generic_string().c_str());
            hitTextures->push_back(SDL_CreateTextureFromSurface(renderer, surf));
            SDL_FreeSurface(surf);
        };
    }

    for (const int &i : lanePosition)
        hits.push_back(std::make_unique<hit::hitPng>(i, redBar.y + (redBar.h / 2), 100, hitTextures));

    auto microSecondsSinceStart = hClock::now();
    std::chrono::microseconds microTimer;

    while (keepGoing) {
        microTimer
            = std::chrono::duration_cast<std::chrono::microseconds>(hClock::now() - microSecondsSinceStart);

        screen.setDrawColor(0, 0, 0, 255);
        screen.clear();

        // Show fps
        if (microTimer.count() >= fpsTimer + 500000) {
            // Destroy before using
            SDL_FreeSurface(surfaceMessageFps);
            SDL_DestroyTexture(messageFps);
            snprintf(fpsString, 20, "FPS: %d", fps);


            surfaceMessageFps = TTF_RenderText_Blended(tuffy, fpsString, { 200, 255, 255, 255 });
            messageFps = SDL_CreateTextureFromSurface(renderer, surfaceMessageFps);

            SDL_QueryTexture(messageFps, nullptr, nullptr, &fpsText->w, &fpsText->h);

            fpsTimer = microTimer.count();
            fps = 0;
        }

        // Redraw measure
        if (processTimingText) {
            SDL_FreeSurface(surfaceMessageTiming);
            SDL_DestroyTexture(messageTiming);


            surfaceMessageTiming = TTF_RenderText_Blended(tuffy, timingString, { 200, 255, 255, 255 });
            messageTiming = SDL_CreateTextureFromSurface(renderer, surfaceMessageTiming);

            SDL_QueryTexture(messageTiming, nullptr, nullptr, &timingText->w, &timingText->h);

            processTimingText = false;
        }

        // FPS
        screen.renderCopy(messageFps, fpsText);

        // Score
        screen.renderCopy(messageScore, scoreText);

        // Timing score
        screen.renderCopy(messageTiming, timingText);

        // Hit
        for (const auto &hitN : hits)
            hitN->render(renderer);


        SDL_SetRenderDrawColor(renderer, 255, 200, 0, 255);

        if (microTimer.count() - nextTarget >= microSecondsDelay) {
            nextTarget = microTimer.count();
            for (auto &i : *noteList) {
                i.rectNote.y += 1; // Hideous
            }
        }

        for (auto it = noteList->begin(); it != noteList->end(); /* We need manual for erase() */) {
            // Don't render when offscreen.
            if (it->rectNote.y <= -25)
                break;
            if (SDL_GetWindowSurface(window) != nullptr
                && it->rectNote.y >= SDL_GetWindowSurface(window)->h) {
                it++;
                continue;
            }

            if (it->bgm && it->rectNote.y >= static_cast<int>(adjustedHeightOfRedBar)) {
                for (const audioChunk &audioStuff : audioChunksList) {
                    if (audioStuff.decodedBase16WavNumber == it->wavNumber) {
                        Mix_PlayChannel(-1, audioStuff.audioChunkForPlaying,
                                        0); // -1 channel = The first available, 0 loops = play once
                        break;
                    }
                }

                it = noteList->erase(it);
            } else {
                // Feels kinda bad to use this shit to color notes. Figure out branchless?
                if (it->rectNote.x == lanePosition[0]) { // Red scratch
                    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
                    SDL_RenderFillRect(renderer, &it->rectNote);
                    SDL_SetRenderDrawColor(renderer, 255, 200, 0, 255);
                } else if (it->rectNote.x == lanePosition[4]) { // Blue spacebar
                    SDL_SetRenderDrawColor(renderer, 60, 70, 225, 255);
                    SDL_RenderFillRect(renderer, &it->rectNote);
                    SDL_SetRenderDrawColor(renderer, 255, 200, 0, 255);
                } else
                    SDL_RenderFillRect(renderer, &it->rectNote);

                it++;
            }
        }


        SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
        SDL_RenderFillRect(renderer, &redBar);


        fps++;

        if (SDL_PollEvent(&events) != 0) { // Needed to update state and this implicitly pumps.
            // KB stuff

            // Update score after the rects have been processed.
            SDL_FreeSurface(surfaceMessageScore);
            SDL_DestroyTexture(messageScore);
            snprintf(scoreString, 20, "Score: %d", score);
            surfaceMessageScore = TTF_RenderText_Blended(tuffy, scoreString, { 200, 255, 255, 255 });
            messageScore = SDL_CreateTextureFromSurface(renderer, surfaceMessageScore);

            SDL_QueryTexture(messageScore, nullptr, nullptr, &scoreText->w, &scoreText->h);
        }

        SDL_RenderPresent(renderer);
    }


    delete fpsText;
    delete timingText;
    delete scoreText;

    for (const auto &i : *hitTextures)
        SDL_DestroyTexture(i);

    for (const auto &i : audioChunksList)
        Mix_FreeChunk(i.audioChunkForPlaying);

    noteList->clear();
    hitTextures->clear();
    hits.clear();

    delete noteList;
    delete hitTextures;

    SDL_FreeSurface(surfaceMessageFps);
    SDL_FreeSurface(surfaceMessageScore);
    SDL_FreeSurface(surfaceMessageTiming);
    SDL_DestroyTexture(messageFps);
    SDL_DestroyTexture(messageScore);
    SDL_DestroyTexture(messageTiming);
    return EXIT_SUCCESS;
}