#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>

#include "sdlScreen.hpp"

sdlScreen::sdlScreen() {
    SDL_Init(SDL_INIT_EVERYTHING);
    TTF_Init();
    IMG_Init(IMG_INIT_PNG);
    Mix_Init(MIX_INIT_OGG);

    SDL_GetCurrentDisplayMode(0, &dm);
    window = SDL_CreateWindow("Wyvern BMS", 0, 0, dm.w, dm.h, SDL_WINDOW_VULKAN | SDL_WINDOW_FULLSCREEN);

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    // TODO: Figure out HiDPI?
    // https://discourse.libsdl.org/t/high-dpi-mode/34411/7
    tuffy = TTF_OpenFont("./tuffy.ttf", 48);

    // Chunksize -> smaller = more responsive, but it may skip if the computer is busy
    //           -> bigger = more delay, less intensive
    Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 1, 2048);
}

SDL_Texture *sdlScreen::createTextureFromSurface(SDL_Surface *surf) {
    return SDL_CreateTextureFromSurface(renderer, surf);
}

void sdlScreen::renderCopy(SDL_Texture *text, SDL_Rect *dst) { SDL_RenderCopy(renderer, text, nullptr, dst); }

void sdlScreen::setDrawColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a) {
    SDL_SetRenderDrawColor(renderer, r, g, b, a);
}

void sdlScreen::drawRect(SDL_Rect *rct) { SDL_RenderFillRect(renderer, rct); }

void sdlScreen::render() { SDL_RenderPresent(renderer); }

void sdlScreen::clear() { SDL_RenderClear(renderer); }

sdlScreen::~sdlScreen() {
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

    SDL_CloseAudio();
    TTF_CloseFont(tuffy);
    Mix_CloseAudio();

    Mix_Quit();
    TTF_Quit();
    IMG_Quit();
    SDL_Quit();
}