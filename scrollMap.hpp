#pragma once

#include <chrono>
#include <vector>

#include <stdint.h>

#include "definitions.hpp"
#include "hitPng.hpp"
#include "sdlScreen.hpp"
#include "wyvernScreen.hpp"

/*
    Class containing the informations relating to a .bms file. It also
    handles the keyboard.

    It does the appropriate calls to a wyvernScreen which does the visual stuff.
*/

class scrollMap {
public:
    scrollMap();

    void processKeyHit(unsigned char lane);

    void pushNote(const note &no);
    void pushName(const name &na);
    void pushAudioChunk(const audioChunk &au);
    void pushHitTexture(SDL_Texture *texture);

    void sortNotes();

    const std::vector<note> &notes();
    const std::vector<name> &names();

    float bpm, spb, spm;

    const uint8_t *currentkb;
    uint8_t oldKb;

private:
    void processKeyNoteClick(int lane); // Only called by processKeyHit()
    bool verifyIfValidHit(const note &no, unsigned char lane);
    void extractTimings();
    void processKeyboard();

    wyvernScreen wScreen;

    hitPng hits;

    std::vector<name> nameList;
    std::vector<audioChunk> audioChunksList;
    std::vector<note> noteList;

    std::chrono::time_point<hClock> startTime, currentTime;
    std::chrono::milliseconds timeSinceStart {};

    int64_t diffTiming {}; // Difference of timing between current note being evaluated and perfect.
    uint64_t score;

    bool keepGoing;
};