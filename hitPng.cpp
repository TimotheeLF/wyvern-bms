#include <iostream>

#include <stdint.h>

#include <SDL2/SDL.h>

#include "hitPng.hpp"
#include "sdlScreen.hpp"

hitPng::hitPng()
    : renderingSteps { 0 }
    , renderedTicks { 0 }
    , currentlyActive { 0 } { }

void hitPng::fillHitRects(int judgementHeight, const std::array<int, 8> &lanePositions) {
    // We give the rects the dimensions from the textures.
    // We assume the first texture has the same dimension as all
    // of the other ones.
    for (size_t i = 0; i < 8; i++) {
        SDL_QueryTexture(hitTextures[0], nullptr, nullptr, &(hitRects[i]->w), &(hitRects[i]->h));
        hitRects[i]->y = judgementHeight - (hitRects[i]->h / 2);
        hitRects[i]->x = lanePositions[i] - (hitRects[i]->w / 2);
    }
}

void hitPng::pushHitTexture(SDL_Texture *texture) { hitTextures.push_back(texture); }

void hitPng::computeSurface(int lane, uint32_t currentTicks) {
    renderedTicks[lane] = currentTicks;
    currentlyActive |= (1 << lane);
}

void hitPng::render(sdlScreen &screen) {
    if (!(currentlyActive & 0)) {
        for (int i = 0; i < 8; i++) {
            if (currentlyActive & (1 << i)) {

                // 20 ms, TODO: Configurable in config file?
                if (SDL_GetTicks() - renderedTicks[i] >= 20) {
                    renderingSteps[i]++;
                    renderedTicks[i] = SDL_GetTicks();
                }

                screen.renderCopy(hitTextures[renderingSteps[i]], hitRects[i]);

                if (renderingSteps[i] >= hitTextures.size()) {
                    renderingSteps[i] = 0;
                    currentlyActive &= ~(1 << i);
                }
            }
        }
    }
}

hitPng::~hitPng() {
    for (auto *i : hitTextures)
        SDL_DestroyTexture(i);
}