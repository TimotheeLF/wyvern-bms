#include <chrono>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <string>
#include <thread>
#include <vector>

#include <stddef.h>
#include <string.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>

// Linux
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include "bmsMap.hpp"
#include "definitions.hpp"

using hClock = std::chrono::high_resolution_clock;

// To set a bit
// |= (1 << LANE1)

// To read a bit
// NOTE: Every single bit in an 8 bit word is evaluated individually (1 to 1)
// EX: 0x00000010 & 0x00000011 -> 0x00000010
//
// & (1 << LANE1);
// Afterwards, you can evaluate if the result is different to 0

/*

BMS FILE FORMAT
https://fileformats.fandom.com/wiki/Be-Music_Script

Note channels:
_________   ___   ___   ___   _______   ___   ___   ___
|scratch|   |s|   |d|   |f|   |space|   |j|   |k|   |l|
‾‾‾‾‾‾‾‾‾   ‾‾‾   ‾‾‾   ‾‾‾   ‾‾‾‾‾‾‾   ‾‾‾   ‾‾‾   ‾‾‾
   16        11    12    13     14       15    18    19

*/

bmsMap::bmsMap(const int scrollingSpeed)
    : lanePosition { 50, 150, 250, 350, 450, 550, 650, 750 }
    , numberOfWavs { 0 } {
    getcwd(cwd, sizeof(cwd));
}

void bmsMap::getMapName() {
    bool isFirstPick { false };
    std::string firstPickName;
    std::string selectedName;

    for (const auto &folder : std::filesystem::directory_iterator(cwd)) {
        std::cout << folder.path().filename() << "\n";

        if (!isFirstPick) {
            isFirstPick = true;
            firstPickName = folder.path().filename();
        }
    }

    std::cout << "Please select the folder (default: " << firstPickName << "):";
    std::getline(std::cin, selectedName);

    if (selectedName.empty())
        selectedName = firstPickName;

    selectedName.insert(0, "/");
    fullBmsPath = cwd + selectedName;

#ifdef DEBUG
    std::cout << "Currently trying to open : " << fullBmsPath << "\n";
#endif
}

void bmsMap::loadNewMap() {
    if (std::filesystem::exists(fullBmsPath)) {
        std::string temporaryString;
        std::string firstString;
        bool firstFolderName = false;

        for (const auto &folder : std::filesystem::directory_iterator(fullBmsPath))
            if (folder.path().extension() == ".bms" || folder.path().extension() == ".bme") {
                std::cout << folder.path().filename() << std::endl;

                if (!firstFolderName) {
                    firstFolderName = true;
                    firstString = folder.path().filename();
                }
            }

        std::cout << "Please choose the .bms or .bme file (default: " << firstString << "): ";
        std::getline(std::cin, temporaryString);

        if (temporaryString.empty())
            temporaryString = firstString;

        temporaryString = fullBmsPath + temporaryString;
        std::string currentLine;

        std::fstream stream;
        stream.open(temporaryString, std::ios::in);

        auto start = hClock::now();

        // Read line by line
        while (std::getline(stream, currentLine)) {
            if (currentLine.length() == 0 || currentLine.starts_with("\r\n")) // Weird DOS thing?
                continue;

            if (currentLine.starts_with("#BPM"))
                handleBpm(currentLine);
            else if (currentLine.starts_with("#WAV"))
                addWavNameToVector(currentLine);
            else if (currentLine.starts_with("#"))
                handleNote(currentLine);
            else {
#ifdef WYVERN_DEBUG
                std::cout << "Ignoring line :" << currentLine << "\n";
#endif
            }
        }

        map.sortNotes();

#ifdef WYVERN_DEBUG
        auto bmsTime = hClock::now();
        std::cout << "Time to parse the BMS file was: "
                  << std::chrono::duration_cast<std::chrono::microseconds>(bmsTime - start).count()
                  << " microseconds" << std::endl;

        std::cout << "The size of the vector is: " << map.notes().size() << std::endl;
#endif

    } else {
        std::cout << "You gave an invalid folder."
                  << "\n";
        return;
    }

    auto wavTime = hClock::now();
    std::vector<std::thread> threads;

    // Load WAVs in parallel because Mix_LoadWAV() sucks ass performance wise.
    threads.reserve(numberOfWavs);
    for (int i = 0; i < numberOfWavs; i++)
        threads.emplace_back(&bmsMap::handleWav, this, i);
    for (std::thread &singleThread : threads)
        singleThread.join();

#ifdef WYVERN_DEBUG
    std::cout << "Time to add WAVs with multithreading was: "
              << std::chrono::duration_cast<std::chrono::milliseconds>(hClock::now() - wavTime).count()
              << " milliseconds" << std::endl;
#endif
}

void bmsMap::loadSkin() {
    for (const auto &file : std::filesystem::directory_iterator(std::string(cwd) + "/skin/")) {
        std::string currentFile = file.path().generic_string();

        if (!file.is_directory() && currentFile.ends_with(".png") && currentFile.starts_with("hit-")) {

            SDL_Surface *surf = IMG_Load(currentFile.c_str());
            map.pushHitTexture(screen.createTextureFromSurface(surf));
            SDL_FreeSurface(surf);
        }
    }
}

void bmsMap::handleNote(const std::string &line) {
    int measure = std::stoi(line.substr(1, 3));
    int lane = convertChannelToLane(std::stoi(line.substr(4, 2)));

    // There are 7 chars before getting to the juicy stuff
    int numbers = static_cast<int>(strcspn(line.c_str(), "\r\n") - 7);

    if (numbers % 2) {
        std::cout << "The decoded amount of numbers isn't pair. Error in parser or the .bms file. "
                     "Skipping the line:\n"
                  << line << "\n";
        return;
    }

    int quantityOfNotesPerMeasure = numbers / 2;


    if (lane != -1) {
        for (int i = 1; i <= quantityOfNotesPerMeasure + 1; i++) { // +1? To double check...
            int index = 7 + 2 * (i - 1);
            if (line.substr(index, index + 2) != "00") {
                int wavNumber = std::stoi(line.substr(index, 2), nullptr, 16);

                // We offset by one the numerator.
                float div = (static_cast<float>(i) - 1.0f) / static_cast<float>(quantityOfNotesPerMeasure);

                float timing = static_cast<float>(measure) + div;

                if (lane != -2) { // -2 is BGM, so no notes
                    map.pushNote({ { lanePosition[7 - lane], 0, 100, 20 },
                                   wavNumber,
                                   timing,
                                   static_cast<uint8_t>(1 << lane),
                                   false });
                } else {
                    map.pushNote({ { 0, 0, 0, 0 }, wavNumber, timing, 0, true });
                }
            }
        }
    }
}

void bmsMap::handleBpm(const std::string &line) {
    line.substr(5, line.size());
    map.bpm = std::stof(line);

#ifdef WYVERN_DEBUG
    std::cout << "Decoded BPM is: " << bpm << std::endl;
#endif
    /*
         Time signatures for individual measures can be specified using the special channel 02.
         Since only one can be assigned in each measure, it only has one value. The value is the
         quotient of the upper number and the lower number. For example, the value for 4/4 is
         1 (default). The value for 3/4 is 0.75. Values are as-is, in decimal (base-10).
         Example: #00302:0.5

         TODO: Extract spm to a per note setting
    */
    map.spb = 60.0f / map.bpm;
    map.spm = map.spb / 4.0f;
}

void bmsMap::handleWav(size_t startIdx) {
#ifdef USE_MMAP_FOR_AUDIO_LOADING
    int fd = open(map.names()[startIdx].nameOfWav.c_str(), O_RDONLY, S_IRUSR | S_IWUSR);
    struct stat sb = { 0 };
    fstat(fd, &sb);

    void *audioFile = mmap(nullptr, sb.st_size, PROT_READ, MAP_PRIVATE, fd, 0);

    map.pushAudioChunk({ Mix_LoadWAV_RW(SDL_RWFromMem(audioFile, static_cast<int>(sb.st_size)), 1),
                         map.names()[startIdx].decodedBase16WavNumber });
    munmap(audioFile, sb.st_size);
    close(fd);
#else
    map.pushAudioChunk({ Mix_LoadWAV(map.names()[startIdx].nameOfWav.c_str()),
                         map.names()[startIdx].decodedBase16WavNumber });
#endif
}

void bmsMap::addWavNameToVector(const std::string &line) {
    int decodedBase16Number = std::stoi(line.substr(4, 2), nullptr, 16);

    std::string nameOfWav = line.substr(7, strcspn(line.c_str(), "\r\n") - 7);

    // TODO: Permanent solution. Check fi file exist, then...
    /*
    char *wavEnding = strstr(temp, "wav"); // BMS is a stupid file format
    if (wavEnding != nullptr)
        strncpy(wavEnding, "ogg", 3);
    else
        std::cout << "Critical error: Couldn't modify string to change wav -> ogg" << std::endl;
    */

    map.pushName({ nameOfWav, decodedBase16Number });
    numberOfWavs++;
}

int bmsMap::convertChannelToLane(int channel) {
    switch (channel) {
    case 11:
        return LANE2;
    case 12:
        return LANE3;
    case 13:
        return LANE4;
    case 14:
        return LANE5;
    case 15:
        return LANE6;
    case 16:
        return LANE1; // Scratch
    case 18:
        return LANE7;
    case 19:
        return LANE8;
    case 01: // BGM
        return -2;
    default:
        return -1;
    }
}
