#pragma once

#include <array>
#include <vector>

#include <stdint.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "sdlScreen.hpp"

class hitPng {
public:
    hitPng();
    ~hitPng();

    void fillHitRects(int judgementHeight, const std::array<int, 8> &lanePositions);
    void pushHitTexture(SDL_Texture *texture);
    void computeSurface(int lane, uint32_t currentTicks);
    void render(sdlScreen &screen);

private:
    std::vector<SDL_Texture *> hitTextures;
    std::vector<SDL_Rect *> hitRects;

    std::array<uint8_t, 8> renderingSteps;
    std::array<uint32_t, 8> renderedTicks;
    uint8_t currentlyActive;
};
