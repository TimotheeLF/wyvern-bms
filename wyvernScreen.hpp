#pragma once

#include <string>

#include <stdint.h>

#include "sdlScreen.hpp"

/*
    Class containing all of the text, SDL_Rects (coordinates) and an sdlScreen.
    This class contains all of the "hardcoded" elements for this game.
*/

class wyvernScreen {
public:
    void updateTimingString(int64_t timing);
    void updateScoreString(uint64_t timing);
    void updateFpsString(uint64_t fps);

    void render();

private:
    sdlScreen scr;

    std::string timingString;
    std::string scoreString;
    std::string fpsString;
};
