#pragma once

#include <stdint.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

/*
    Small class abstracting over SDL stuff.
*/

class sdlScreen {
public:
    sdlScreen();
    ~sdlScreen();

    SDL_Texture *createTextureFromSurface(SDL_Surface *surf);
    void renderCopy(SDL_Texture *text, SDL_Rect *dst);
    void setDrawColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
    void drawRect(SDL_Rect *rct);
    void render();
    void clear();

private:
    SDL_Window *window;
    SDL_Renderer *renderer;
    SDL_Event events;
    SDL_DisplayMode dm;
    TTF_Font *tuffy;
};